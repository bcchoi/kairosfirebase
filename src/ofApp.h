#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "fsm/state_context.h"

#define SELFPORT 12352

class ofApp : public ofBaseApp {

	// communication
	ofxOscReceiver receiver;
	// state
	itg::ofxStateMachine<Context> stateMachine;

public:
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
};
