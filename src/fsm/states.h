/*
by bcc
2017.05.12
*/

#pragma once
#include "state_context.h"
#include "Poco/Base64Encoder.h"

using namespace std;

char* unicodeToUtf8(const wchar_t* unicode) {
	char* strUtf8 = new char[256];
	memset(strUtf8, 0, 256);
	int nLen = WideCharToMultiByte(CP_UTF8, 0, unicode, lstrlenW(unicode), NULL, 0, NULL, NULL);
	WideCharToMultiByte(CP_UTF8, 0, unicode, lstrlenW(unicode), strUtf8, nLen, NULL, NULL);
	return strUtf8;
}
wchar_t* utf8ToUnicode(const char* utf8) {
	wchar_t* strUnicode = new wchar_t[256];
	memset(strUnicode, 0, 256);
	char	strUTF8[256] = { 0, };
	strcpy_s(strUTF8, 256, utf8);
	int nLen = MultiByteToWideChar(CP_UTF8, 0, strUTF8, strlen(strUTF8), NULL, NULL);
	MultiByteToWideChar(CP_UTF8, 0, strUTF8, strlen(strUTF8), strUnicode, nLen);
	return strUnicode;
}

template <typename A, typename B>
multimap<B, A> flip_map(map<A, B> & src) {

	multimap<B, A> dst;

	for (map<A, B>::const_iterator it = src.begin(); it != src.end(); ++it)
		dst.insert(pair<B, A>(it->second, it->first));

	return dst;
}

class ofxUserState : public itg::ofxState<Context>{
protected:
public:
	virtual string getName() { return ""; };
	virtual void stateEnter() {
		Context& ctx = getSharedData();
		ofLogNotice("current state") << getName();
		ctx.previousState = ctx.currentState;
		ctx.currentState = getName();
	}
	virtual void draw() {
		Context& ctx = getSharedData();
		ofDrawBitmapString(ctx.ss.str(), 5, 10);
	}
};

class FaceImage : public ofxUserState {
	ofApp* app;
	int face_image_timeout;
	int save_image_timeout;
	ofxRemoteOfImage client;
	ofImage receivedImage;	
	std::vector<ofPixels> faces;
public:
	string getName() { return "faceimage"; }
	void setup() {
		ofLogNotice("loading") << getName();
		app = (ofApp*)getSharedData().app;
		save_image_timeout = 0;
	}		
	void stateEnter() {
		ofxUserState::stateEnter();
		face_image_timeout = ofGetElapsedTimeMillis();

		client.setDesiredFramerate(5);
		client.setNetworkProtocol(REMOTE_OF_IMAGE_TCP);
		client.connect(&receivedImage, "127.0.0.1");

		faces.clear();	// init
	}
	void update() {
		if (ofGetElapsedTimeMillis() - face_image_timeout > 5000) {
			changeState("detect");
			return;
		}
	}
	void draw() {		
		client.begin(); //call begin() before drawing the image, to avoid tearing

		client.updatePixels();	//if you need to draw the image on screen, call updatePixels() before drawing it
								//otherwise the texture will not be allocated to save on GPU time

		if (receivedImage.isAllocated()) {			
			if (ofGetElapsedTimeMillis() - save_image_timeout > (1000/2)) {
				save_image_timeout = ofGetElapsedTimeMillis();
				// save image
				faces.push_back(receivedImage.getPixels());				
			}
		}

		for (int i = 0; i < faces.size(); i++) {
			ofTexture texture;			
			texture.loadData(faces[i]);
			int x = i;
			int y = 0;
			if (i > 4) {
				x -= 5;
				y = 120;
			}
			texture.draw(x * 120, y, 120, 120); //draw your image normally				
		}
		
		client.end(); //call end() after drawing the image, to avoid tearing
	}
	void stateExit() {
		client.stop();
		Context& ctx = getSharedData();
		ctx.recogFaces.clear();
		ctx.enrollFaces.clear();
		ctx.detectFaces.clear();

		ctx.folder = ofToString(ofGetUnixTime());
		ofDirectory::createDirectory(ctx.folder);

		for (int i = 0; i < faces.size(); i++) {
			ctx.recogFaces.push_back(faces[i]);
			ctx.enrollFaces.push_back(faces[i]);
			ctx.detectFaces.push_back(faces[i]);
			
			// for check
			ofSaveImage(faces[i], ctx.folder + "/" + ctx.folder + "g" + ofToString(i) + ".png");
		}		
		// old image comes in first image
		/// std::random_shuffle(ctx.recogFaces.begin(), ctx.recogFaces.end());
	}
};

class Recognize : public ofxUserState {
	ofApp* app;	
public:
	string getName() { return "recognize"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	
	void stateEnter() {
		ofxUserState::stateEnter();
		Context& ctx = getSharedData();

		// test
		/*ofDirectory dir;
		ctx.folder = "1495616535";
		dir.listDir(ofToDataPath(ctx.folder, true));
		ctx.recogFaces.clear();
		for (int i = 0; i < dir.numFiles(); i++) {
			string fname = dir.getFile(i).getFileName();			
			ofLog() << fname;
			ofImage image;
			image.load(dir.getPath(i));
			ctx.recogFaces.push_back(image.getPixels());
		}*/
	}
	void update() {
		Context& ctx = getSharedData();
		ctx.ss.str(""); ctx.ss.clear();
		// 5 of 10 to reduce working time
		if (ctx.recogFaces.size() == 0) changeState("enroll"); 
		if (ctx.recogFaces.size()) {
			// make buffer
			ofBuffer ib;
			ofSaveImage(ctx.recogFaces.back(), ib);
			std::stringstream ss;
			Poco::Base64Encoder b64encoder(ss);
			b64encoder << ib;

			// set header
			ofxHttpForm header;
			header.addHeaderField("app_id", "4b9916e8");
			header.addHeaderField("app_key", "c025c6e0521d9d4148e5b0477e3d5168");

			// set body
			ofxJSONElement json;
			json["minHeadScale"] = ".5";
			json["gallery_name"] = "testgallery";
			json["image"] = ss.str();
			ofBuffer body;
			body.set(json.getRawString());
			ctx.ss << getName() << " posting... " << ctx.recogFaces.size() << endl;
			std::string url = "https://api.kairos.com/recognize";
			ofxHttpResponse &response = ctx.httpUtils.postData(url, header, body, "application/json");
			if (response.responseBody.size() > 0) {
				ofxJSONElement body;
				body.parse(response.responseBody);

				if (body["images"].isArray()) {
					ofxJSONElement transaction = body["images"][0]["transaction"];
					ofxJSONElement candidates = body["images"][0]["candidates"];
					if (transaction.isNull() || candidates.isNull()) {
						ctx.recogFaces.pop_back();
						return;
					}
					if (transaction["status"].asString() == "success") {
						// debugging
						ofxJSONElement json;
						json["gallery_name"] = transaction["gallery_name"].asString();
						json["subject_id"] = transaction["subject_id"].asString();
						json["confidence"] = transaction["confidence"].asDouble();
						ctx.ss << json.getRawString() << endl;

						if (candidates.size()) {
							// first candidate
							ctx.currentUserInfo["subject_id"] = candidates[0]["subject_id"].asString();
							changeState("getuser");
						}
					}
				}

				if (body["Errors"].isArray()) {
					int error = body["Errors"][0]["ErrCode"].asInt();
					ctx.ss << "error: " << error << endl;
					if (error == 5004 )
						changeState("enroll");
					if (error == 5002)
						changeState("retryfaceimage");
				}
			}
			else {
				ctx.ss << "retry..." << endl;
			}
			
			ctx.recogFaces.pop_back();
		}		
	}	
};

class GetUser : public ofxUserState {
public:
	string getName() { return "getuser"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void update() {
		Context& ctx = getSharedData();
		ofxOscMessage m;
		m.setAddress("/watsonkiosk");		// to
		m.addStringArg("/kairosfirebase");	// from
		m.addStringArg("useridentity");		

		ofxPythonObject func = ctx.script.attr("get_user");
		std::string id = ctx.currentUserInfo["subject_id"].asString();
		if (func) {
			ofxPythonObject obj = func(ofxPythonObject::fromString(id));
			std::vector<ofxPythonObject>& user = obj.asVector();
			if (user.size()) {
				std::string key = user[0].asString();				
				std::string data = user[1].asString();
				m.addStringArg(data);
				ctx.currentKey = key;
				ctx.watsonKioskSender.sendMessage(m);
			}
			else {				
				func = ctx.script.attr("create_user");
				std::string data = ctx.currentUserInfo.getRawString();
				if (func) {
					func(ofxPythonObject::fromString(data));
					m.addStringArg(data);					
					ctx.watsonKioskSender.sendMessage(m);
				}
			}
		}		
		changeState("untildone");
	}
};



class Enroll : public ofxUserState {
	ofApp* app;
	std::vector<std::string> filename;
public:
	string getName() { return "enroll"; }
	void setup() {
		ofLogNotice("loading") << getName();
		app = (ofApp*)getSharedData().app;
	}
	void stateEnter() {
		ofxUserState::stateEnter();
		Context& ctx = getSharedData();

		// test
		/*ofDirectory dir;	
		ctx.folder = "1495616535";
		dir.listDir(ofToDataPath(ctx.folder, true));
		ctx.enrollFaces.clear();
		for (int i = 0; i < dir.numFiles(); i++) {
			string fname = dir.getFile(i).getFileName();
			ofLogNotice() << fname;
			ofImage image;
			image.load(dir.getPath(i));
			ctx.enrollFaces.push_back(image.getPixels());
			filename.push_back(fname.substr(0, fname.size() - 4));
		}*/

		// filename setup
		ofDirectory dir;
		dir.listDir(ofToDataPath(ctx.folder, true));
		for (int i = 0; i < dir.numFiles(); i++) {
			string fname = dir.getFile(i).getFileName();
			filename.push_back(fname.substr(0, fname.size() - 4));
		}
	}
	void update() {
		Context& ctx = getSharedData();	
		ctx.ss.str(""); ctx.ss.clear();
		// What if I can not register all images? It is less likely.
		// It's already processed by Error.
		// so compelete encoll image
		if (ctx.enrollFaces.size() == 0) {
			// create user on firebase
			ofxPythonObject func = ctx.script.attr("create_user");
			std::string data = ctx.currentUserInfo.getRawString();
			if (func) {
				ofxPythonObject obj = func(ofxPythonObject::fromString(data));
				ofxOscMessage m;
				m.setAddress("/watsonkiosk");		// to
				m.addStringArg("/kairosfirebase");	// from
				m.addStringArg("firstvisitors");
				m.addStringArg(data);
				ctx.watsonKioskSender.sendMessage(m);
			}

			changeState("untildone");
		}
		if (ctx.enrollFaces.size()) {
			// make buffer
			ofBuffer ib;
			ofSaveImage(ctx.enrollFaces.back(), ib);
			std::stringstream ss;
			Poco::Base64Encoder b64encoder(ss);
			b64encoder << ib;

			// set header
			ofxHttpForm header;
			header.addHeaderField("app_id", "4b9916e8");
			header.addHeaderField("app_key", "c025c6e0521d9d4148e5b0477e3d5168");

			// set body
			ofxJSONElement json;
			json["subject_id"] = filename.back();
			json["gallery_name"] = "testgallery";
			json["minHeadScale"] = ".5";
			json["multiple_faces"] = "false";
			json["image"] = ss.str();
			ofBuffer body;
			body.set(json.getRawString());	
			ctx.ss << getName() << " posting... " << ctx.enrollFaces.size() << endl;
			std::string url = "https://api.kairos.com/enroll";
			ofxHttpResponse &response = ctx.httpUtils.postData(url, header, body, "application/json");
			if(response.responseBody.size() > 0) {
				ofxJSONElement body;
				body.parse(response.responseBody);

				if (body["images"].isArray()) {
					ofxJSONElement attributes = body["images"][0]["attributes"];
					ofxJSONElement transaction = body["images"][0]["transaction"];
					if (transaction.isNull() || attributes.isNull()) {
						ctx.enrollFaces.pop_back();
						filename.pop_back();
						return;
					}
					if (transaction["status"].asString() == "success") {

						std::string gender = attributes["gender"]["type"].asString();
						int age = attributes["age"].asInt();
						std::string glasses = attributes["glasses"].asString();
						std::map<std::string, double> color;
						color["asian"] = attributes["asian"].asDouble();
						color["hispanic"] = attributes["hispanic"].asDouble();
						color["other"] = attributes["other"].asDouble();
						color["black"] = attributes["black"].asDouble();
						color["white"] = attributes["white"].asDouble();

						multimap<double, std::string> reverseColor = flip_map(color);
						multimap<double, string>::const_reverse_iterator it = reverseColor.rbegin();

						ofxJSONElement json;
						json["subject_id"] = transaction["subject_id"].asString();
						json["gallery_name"] = transaction["gallery_name"].asString();
						json["gender"] = gender;
						json["age"] = age;
						json["color"] = it->second;
						json["glasses"] = glasses;

						ctx.ss << json.getRawString() << endl;
						ctx.currentUserInfo = json;						
					}
				}

				if (body["Errors"].isArray()) {
					int error = body["Errors"][0]["ErrCode"].asInt();
					ctx.ss << "error: " << error << endl;
					if (error == 5002)
						changeState("retryfaceimage");
				}
			}
			else ctx.ss << "retry..." << endl;
			
			ctx.enrollFaces.pop_back();
			filename.pop_back();
		}
	}	
};

class Detect: public ofxUserState {
	ofApp* app;
public:
	string getName() { return "detect"; }
	void setup() {
		ofLogNotice("loading") << getName();
		app = (ofApp*)getSharedData().app;
	}
	void stateEnter() {
		ofxUserState::stateEnter();
		Context& ctx = getSharedData();
		
		// test
		/*ofDirectory dir;
		ctx.folder = "1495616535";
		dir.listDir(ofToDataPath(ctx.folder, true));
		ctx.detectFaces.clear();
		for (int i = 0; i < dir.numFiles(); i++) {
			string fname = dir.getFile(i).getFileName();
			ofLog() << fname;
			ofImage image;
			image.load(dir.getPath(i));
			ctx.detectFaces.push_back(image.getPixels());
		}*/		
	}

	void update() {
		Context& ctx = getSharedData();
		ctx.ss.str(""); ctx.ss.clear();
		// 5 of 10 to reduce working time
		if (ctx.detectFaces.size() == 0) changeState("retryfaceimage"); 
		if (ctx.detectFaces.size()) {
			// make buffer
			ofBuffer ib;
			ofSaveImage(ctx.detectFaces.back(), ib);
			std::stringstream ss;
			Poco::Base64Encoder b64encoder(ss);
			b64encoder << ib;

			// set header
			ofxHttpForm header;
			header.addHeaderField("app_id", "4b9916e8");
			header.addHeaderField("app_key", "c025c6e0521d9d4148e5b0477e3d5168");

			// set body
			ofxJSONElement json;
			json["minHeadScale"] = ".5";
			json["selector"] = "ROLL";
			json["image"] = ss.str();
			ofBuffer body;
			body.set(json.getRawString());
			ctx.ss << getName() << " posting... " << ctx.detectFaces.size() << endl;
			std::string url = "https://api.kairos.com/detect";
			ofxHttpResponse &response = 
				ctx.httpUtils.postData(url, header, body, "application/json");
			if (response.responseBody.size() > 0) {
				ofxJSONElement body;
				body.parse(response.responseBody);
				
				if (body["images"].isArray()) {
					ofxJSONElement faces = body["images"][0]["faces"];

					if (faces.isNull()) {
						ctx.detectFaces.pop_back();
						return;
					}
					ofxJSONElement attributes = faces[0]["attributes"];
					std::string gender = attributes["gender"]["type"].asString();
					int age = attributes["age"].asInt();
					std::string glasses = attributes["glasses"].asString();
					std::map<std::string, double> color;
					color["asian"] = attributes["asian"].asDouble();
					color["hispanic"] = attributes["hispanic"].asDouble();
					color["other"] = attributes["other"].asDouble();
					color["black"] = attributes["black"].asDouble();
					color["white"] = attributes["white"].asDouble();

					multimap<double, std::string> reverseColor = flip_map(color);
					multimap<double, string>::const_reverse_iterator it = reverseColor.rbegin();

					ofxJSONElement json;
					json["gender"] = gender;
					json["age"] = age;
					json["color"] = it->second;
					json["glasses"] = glasses;

					ctx.ss << json.getRawString() << endl;
					ctx.currentUserInfo = json;

					ofxOscMessage m;
					m.setAddress("/watsonkiosk");		// to
					m.addStringArg("/kairosfirebase");	// from
					m.addStringArg("facedetected");
					m.addStringArg(json.getRawString());
					ctx.watsonKioskSender.sendMessage(m);
				
					changeState("recognize");
				}

				if (body["Errors"].isArray()) {
					int error = body["Errors"][0]["ErrCode"].asInt();					
					ctx.ss << "error: " << error << endl;

					if (error == 5002)
						changeState("retryfaceimage");
				}
			}
			else {
				ctx.ss << "retry..." << endl;
			}
			ctx.detectFaces.pop_back();
		}
	}	
};

class RetryFaceImage : public ofxUserState {
public:
	string getName() { return "retryfaceimage"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void update() {
		changeState("idle");
	}
};

class UserUpdate: public ofxUserState {

public:
	string getName() { return "userupdate"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void update() {
		Context& ctx = getSharedData();
		// update on firebase
		ofxPythonObject func = ctx.script.attr("update_user");
		std::string userid = "";
		std::string data = ctx.currentUserInfo.getRawString();
		if (func) {
			ofxPythonObject obj =
				func(
					ofxPythonObject::fromString(userid),
					ofxPythonObject::fromString(data)
				);
		}
		changeState("idle");
	}
};

class Idle : public ofxUserState {
	int lastUpdate;
public:
	string getName() { return "idle"; }
	void setup() {
		ofLogNotice("loading") << getName();
		lastUpdate = 0;
	}

	void update() {
		Context& ctx = getSharedData();

		if (ofGetElapsedTimeMillis() - lastUpdate > 5000) {
			lastUpdate = ofGetElapsedTimeMillis();
			ofxOscMessage m;
			m.setAddress("/watsonkiosk");		// to
			m.addStringArg("/kairosfirebase");	// from
			m.addStringArg("ping");
			ctx.watsonKioskSender.sendMessage(m);
			ctx.alive = false;
		}
	}

	void keyPressed(int key) {
		// test
		Context& ctx = getSharedData();
		if (key == '1') {		
			ofxOscMessage m;
			m.setAddress("/watsonkiosk");		// to
			m.addStringArg("/kairosfirebase");	// from
			m.addStringArg("facedetected");
			char* str = unicodeToUtf8(L"�ֺ�ö");
			m.addStringArg(str);
			ctx.watsonKioskSender.sendMessage(m);
			delete[] str;
		}
		else if (key == '2') {
			int elapsed = ofGetElapsedTimeMillis();
			ofxPythonObject func = ctx.script.attr("get_user_by_id");			
			if (func) {
				ofxPythonObject obj = func(ofxPythonObject::fromString("1487012582681"));
				std::vector<ofxPythonObject>& user = obj.asVector();
				ofLog() << ofGetElapsedTimeMillis() - elapsed << "msec";
				for (std::vector<ofxPythonObject>::iterator it = user.begin(); it != user.end(); ++it) {
					ofLog() << it->repr();
				}
			}

			func = ctx.script.attr("get_user_test");
			if (func) {
				func();
			}
		}
		else if (key == '3') {
			int elapsed = ofGetElapsedTimeMillis();
			ofxPythonObject func = ctx.script.attr("get_user");
			if (func) {
				ofxPythonObject obj = func(ofxPythonObject::fromString("1487012582682"));
				std::vector<ofxPythonObject>& user = obj.asVector();
				ofLog() << ofGetElapsedTimeMillis() - elapsed << "msec";
				for (std::vector<ofxPythonObject>::iterator it = user.begin(); it != user.end(); ++it) {
					ofLogNotice("c++") << it->asString();
				}
				
				std::string key = user[0].asString();
				std::string data = user[1].asString();
				ofxJSONElement json;
				json.parse(data);

				ofLog() << json["color"].asString();
				ofxJSONElement order = json["order"];
				if(!order.isNull())
					ofLog() << order["drink"].asString();

				func = ctx.script.attr("get_user_by_key");
				if (func) {
					func(ofxPythonObject::fromString(key));
				}
			}
		}
		else if (key == '4') {
			int elapsed = ofGetElapsedTimeMillis();
			ofxPythonObject func = ctx.script.attr("create_user");
			ofxJSONElement json;
			json["subject_id"] = "11415123412";
			json["gallery_name"] = "testgallery";
			json["gender"] = "F";
			json["age"] = 44;
			json["color"] = 0.764;
			json["glasses"] = "None";
			ctx.currentUserInfo = json;
			std::string data = ctx.currentUserInfo.getRawString();
			if (func) {
				ofxPythonObject obj = func(ofxPythonObject::fromString(data));
				ofLog() << ofGetElapsedTimeMillis() - elapsed << "msec";
			}
		}
		else if (key == '5') {
			int elapsed = ofGetElapsedTimeMillis();
			ofxPythonObject func = ctx.script.attr("patch_user");
			ofxJSONElement json;
			
			json["age"] = 12;
			ctx.currentUserInfo = json;
			std::string data = ctx.currentUserInfo.getRawString();
			if (func) {
				ofxPythonObject obj = func(
					ofxPythonObject::fromString("-KkaUjXGmdLjLHNWfFwK"),
					ofxPythonObject::fromString(data)					
					);
				ofLog() << ofGetElapsedTimeMillis() - elapsed << "msec";
			}
		}
		else if (key == '6') {
			int elapsed = ofGetElapsedTimeMillis();
			ofxPythonObject func = ctx.script.attr("put_order");
			ofxJSONElement json;
			json["menu"] = "1set";
			json["drink"] = "water";
			json["side"] = "frid";
			json["card"] = "12552315123523";			
			ctx.currentUserInfo = json;
			std::string data = ctx.currentUserInfo.getRawString();
			if (func) {
				ofxPythonObject obj = func(
					ofxPythonObject::fromString("-KkaUjXGmdLjLHNWfFwK"),
					ofxPythonObject::fromString(data)
				);
				ofLog() << ofGetElapsedTimeMillis() - elapsed << "msec";
			}
		}
	}
};

class ReadyFaceImage : public ofxUserState {
	int delay;
public:
	string getName() { return "readyfaceimage"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		ofxUserState::stateEnter();
		Context& ctx = getSharedData();

		ofxOscMessage m;
		m.setAddress("/watsonkiosk");		// to
		m.addStringArg("/kairosfirebase");	// from
		m.addStringArg("lookatme");
		ctx.watsonKioskSender.sendMessage(m);

		delay = ofGetElapsedTimeMillis();
	}
	void update() {
		Context& ctx = getSharedData();

		// run after "loot at me" working time
		if (ofGetElapsedTimeMillis() - delay > 2000) {
			ofxOscMessage m;
			m.setAddress("/realsense");			// to
			m.addStringArg("/kairosfirebase");	// from
			m.addStringArg(getName());
			ctx.realsenseSender.sendMessage(m);
			changeState("idle");
		}
	}
};

class UntilDone : public ofxUserState {
	int lastUpdate;
public:
	string getName() { return "untildone"; }
	void setup() {
		ofLogNotice("loading") << getName();
		lastUpdate = 0;
	}
	void keyPressed(int key) {
		if (key == '1') {
			changeState("idle");
		}
	}
};

class Pong : public ofxUserState {

public:
	string getName() { return "pong"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		ofxUserState::stateEnter();
		Context& ctx = getSharedData();
		ctx.alive = true;
	}
	void update() {
		changeState("idle");
	}
};
