
/*
by bcc
2017.05.12
*/
#include "state_context.h"
#include "ofApp.h"

void Context::setup() {
	currentKey = "";
	alive = false;
	currentState = "";
	previousState = "";
	folder = "";
	responseBody = "";
	watsonKioskSender.setup("localhost", WATSONKIOSKPORT);
	realsenseSender.setup("localhost", REALSENSEPORT);
	python.init();
	python.executeScript("worker.py");
	ofxPythonObject klass = python.getObject("KairosFirebase");
	script = klass();
	httpUtils.start();
}