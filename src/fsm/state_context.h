

/*
by bcc
2017.05.12
*/

#pragma once
#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxStateMachine.h"
#include "ofxRemoteOfImage.h"
#include "ofxJSON.h"
#include "ofxPython.h"
#include "ofxHttpUtils.h"

#define WATSONKIOSKPORT 12345
#define REALSENSEPORT 12347

class ofxUserListener {
public:
	virtual ~ofxUserListener() {}
	virtual void stateHandler(std::string& arg) {}
};
class Context {
public:
	void setup();
	void update();

	// system
	ofBaseApp* app;
	std::string currentState;
	std::string previousState;

	// save image
	std::vector<ofPixels> recogFaces;
	std::vector<ofPixels> enrollFaces;
	std::vector<ofPixels> detectFaces;
	
	// folder name;
	std::string folder;

	// osc setup
	ofxOscSender watsonKioskSender;
	ofxOscSender realsenseSender;
	// python
	std::string responseBody;
	ofxPython python;
	ofxPythonObject script;

	std::stringstream ss;

	// kairos server
	ofxHttpUtils httpUtils;
	// firebase
	ofxJSONElement currentUserInfo;
	std::string currentKey;

	// watson&kiosk
	bool alive;



};
