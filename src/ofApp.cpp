#include "ofApp.h"
#include "Poco/Base64Encoder.h"
#include "fsm/states.h"
//--------------------------------------------------------------
void ofApp::setup(){
	receiver.setup(SELFPORT);
	stateMachine.getSharedData().app = this;
	stateMachine.getSharedData().setup();
	stateMachine.addState<FaceImage>();
	stateMachine.addState<Recognize>();
	stateMachine.addState<Enroll>();
	stateMachine.addState<Idle>();
	stateMachine.addState<RetryFaceImage>();
	stateMachine.addState<Detect>();
	stateMachine.addState<UntilDone>();
	stateMachine.addState<UserUpdate>();
	stateMachine.addState<Pong>();
	stateMachine.addState<ReadyFaceImage>();
	stateMachine.addState<GetUser>();
	stateMachine.changeState("idle");
}

//--------------------------------------------------------------
void ofApp::update(){
	Context& ctx = stateMachine.getSharedData();

	while (receiver.hasWaitingMessages()) {
		ofxOscMessage m;
		receiver.getNextMessage(m);
		if (m.getAddress() == "/kairosfirebase") {
			if (m.getArgAsString(0) == "/realsense") {
				if (m.getArgAsString(1) == "faceimage") {
					if(ctx.currentState == "idle" && ctx.alive) // if watson&kiosk is alive
						stateMachine.changeState("faceimage");
				}	
				else if (m.getArgAsString(1) == "facedetected") {
					if (ctx.currentState == "idle" && ctx.alive) {
						stateMachine.changeState("readyfaceimage");
					}
				}
			}					
			else if (m.getArgAsString(0) == "/watsonkiosk") {
				if (m.getArgAsString(1) == "untildone") {
					// set update data
					stateMachine.changeState("userupdate");	// userupdate -> idle
				}
				else if (m.getArgAsString(1) == "pong") {
					stateMachine.changeState("pong");
				}
			}
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw(){

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}
