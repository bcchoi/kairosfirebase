#include "ofMain.h"
#include "ofApp.h"
#include "ofAppNoWindow.h"
//========================================================================
int main() {
#ifdef NDEBUG
	ofAppNoWindow window;
	ofSetupOpenGL(&window, 300, 300, OF_WINDOW);
	ofRunApp(new ofApp());
#else

	ofAppGlutWindow window;
	ofSetupOpenGL(&window, 600, 240, OF_WINDOW);			// <-------- setup the GL context
															//ofSetupOpenGL(600, 240, OF_WINDOW);			// <-------- setup the GL context

															// this kicks off the running of my app
															// can be OF_WINDOW or OF_FULLSCREEN
															// pass in width and height too:
	ofRunApp(new ofApp());
#endif
}
