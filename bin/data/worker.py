

import json, time
from openframeworks import *
from firebase import firebase

class KairosFirebase(object):
	firebase = firebase.FirebaseApplication('https://watson-cd664.firebaseio.com/', None)

	def __init__(self):
		pass

	def get_user_by_key(self, key):
		path = '/users/{key}'.format(key=key)
		r = self.firebase.get(path, None)
		print r

	def get_user(self, id):
		r = self.firebase.get('/users', None)
		for key, value in r.items():
			rid = r[key]['subject_id']
			if id == rid:
				self.key = key
				return [key.encode('euc-kr'), json.dumps(r[key])]
		return []

	def create_user(self, data):
		o = json.loads(data)
		r = self.firebase.post('/users', o)
		print r

	def put_order(self, key, data):
		o = json.loads(data)
		path = '/users/{key}'.format(key=key)
		r = self.firebase.put(path, "order", o)
		print r

	def put_emotion(self, key, data):
		o = json.loads(data)
		path = '/users/{key}/'.format(key=key)
		r = self.firebase.patch(path, "emotion", o)
		print r


	def patch_user(self, key, data):
		o = json.loads(data)
		path = '/users/{key}'.format(key=key)
		r = self.firebase.patch(path, o)
		print r

	def get_user_test(self):
		path = '/users/{key}'.format(key=self.key)
		r = self.firebase.get(path, None)
		print json.dumps(r)


	def get_uses(self):
		result = self.firebase.get('/users', None)
		print(json.dumps(result, indent=2))

		for key, value in result.items():
			id = result[key]['subject_id']
			if id == '1487012582681':
				return [key, json.dumps(result[key])]
			user = result[key]
			print type(user)
			print type(id)
			print type(json.dumps(id))
			print json.dumps(id)
			print id.encode('utf-8')

		#	return [key1, result[key1]['age']]

		#for key, value in result.items():
		#	print type(value)
		#	if value['subject_id'] == '1487012582681':
		#		print 'goog'


		return []


